import styled from 'styled-components';

export default styled.button`
    border-radius: .5em;
    text-transform: uppercase;
    outline: none;
    border: none;
    height: 50px;
    min-width: 120px;
    background-color: #4A4A4A;
    color: #fff;
    font-size: 18px;
    padding: 0 12px;

    &:hover {
        background-color: #7E7C7C;
    }
`;