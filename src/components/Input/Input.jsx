import React from 'react';
import styled from 'styled-components';

const Input = styled.input`
    height: 50px;
    border-radius: 3px;
    outline: none;
    border: none;
    background-color: #DEDDDD;
    padding: 6px 10px 0 17px;
    box-sizing: border-box;
    color: #898686;
    font-size: 18px;
    line-height: 1.5;

    &::placeholder {
        color: transparent;
    }

    &:not(:placeholder-shown) ~ .form__label {
        cursor: text;
        position: absolute;
        top: 4px;
        left: 30px;
        display: block;
        transition: 0.2s;
        font-size: .7rem;
        opacity: .7;
    }

    &:focus {
        ~ .form__label {
            position: absolute;
            top: 4px;
            left: 30px;
            display: block;
            transition: 0.2s;
            font-size: .7rem;
            opacity: .7;
        }
        border-width: 3px;
    }

    &:required, &:invalid { box-shadow:none; }
`;

const WrapInput = styled.div`
    position: relative;
    padding: 0 8px;
    box-sizing: border-box;
    height: 50px;
    flex: 0 0 30%;
`;

const Label = styled.label`
    position: absolute;
    display: block;
    transition: 0.2s;
    font-size: 1.1rem;
    color: #898686;
    top: 15px;
    left: 30px;
    cursor: text;
    pointer-events: none;
`;

const InputComponent = ({ changeInput }) => {
    const handleChange = (event) => changeInput(event.target.value);
    return (
    <WrapInput>
        <Input placeholder={'Enter your App id'} onChange={handleChange} id="id-input" pattern="\d*" required />
        <Label forHTML="id-input" className='form__label'>Enter your App id</Label>
    </WrapInput>
    )
}

export default InputComponent;