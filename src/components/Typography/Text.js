import styled from 'styled-components';

export default styled.span`
    color: ${(props) => props.color  ? props.color : '#615F5F'};
    font-size: ${(props) => props.fs ? props.fs + 'px' : '20px'};
    line-height: 1.5;
    margin-bottom: ${(props) => props.mb ? props.mb + 'px' : ''};
`;