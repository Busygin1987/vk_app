import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { useHistory } from "react-router-dom";

import { handleLogin } from './actions';
import Input from '../../components/Input';
import Button from '../../components/Button';
import Loader from '../../components/Loader';
import { Text } from '../../components/Typography';

const WrapPage = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    flex-direction: column;
`;

const Block = styled.div`
    display: flex;
    justify-content: center;
    margin: 24px 0 0 0;
`;

const Auth = () => {
    const [ input, setInput ] = React.useState('');
    const dispatch = useDispatch();
    const history = useHistory();

    const { loading, login, error } = useSelector(({ auth: { authentication, login, error } }) => ({
        loading: authentication,
        login,
        error,
    }));

    React.useEffect(() => {
        login && history.push('/home');
    }, [ history, login ]);

    const handleClick = () => input && dispatch(handleLogin(input));

    return (
        <WrapPage>
            { error ?
                <Text>{error}</Text>
                : loading
                    ? <Loader />
                    : <>
                        <Text>Enter your id to login in App</Text>
                        <Block>
                            <Input changeInput={setInput} />
                            <Button onClick={handleClick}>Login</Button>
                        </Block>
                    </>
            }
        </WrapPage>
    )
}

export default Auth;