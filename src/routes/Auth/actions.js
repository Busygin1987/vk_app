/* eslint-disable no-undef */
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';

export const handleLogin = (id) => {
    return (dispatch) => {
        dispatch({
            type: LOGIN_REQUEST,
        });

        VK.init({
            apiId: id
        });

        VK.Auth.login(r => {
            console.log(r)
            if (r.session) {
                const user = r.session.user;
                const name = `${user.first_name} ${user.last_name}`;

                localStorage.setItem('logged_in', true);
                localStorage.setItem('name', name);
                dispatch({
                    type: LOGIN_SUCCESS,
                    payload: user,
                })
            } else {
                dispatch({
                    type: LOGIN_FAIL,
                    payload: new Error('Ошибка авторизации'),
                })
            }
        }, 4);
    }
}