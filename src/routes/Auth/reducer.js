import { createReducer } from '../../store/utils/createReducer';
import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAIL
} from './actions';

const initialState = {
    authentication: false,
    error: '',
    login: false,
    logout: false,
    user: {},
}

export default createReducer({
    [LOGIN_REQUEST]: (state) => ({
        ...state,
        authentication: true
    }),
    [LOGIN_SUCCESS]: (state, payload) => ({
        ...state,
        authentication: false,
        login: true,
        user: payload,
    }),
    [LOGIN_FAIL]: (state, payload) => ({
        ...state,
        error: payload,
        authentication: false,
    }),
}, initialState);