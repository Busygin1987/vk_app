import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { Link } from "react-router-dom";

import Image from '../../components/Image';
import { Text } from '../../components/Typography';

const WrapPage = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const WrapPhoto = styled.div`
    padding: 10px;
    border: 1px solid #000;
    border: 4px;
    width: 100px;
    height: 100px;
`;

const Album = () => {
    const photos = useSelector(({ home: { photos } }) => photos );

    return (
        <WrapPage>
            {photos.map((photo) => {
                const size = photo.sizes.length - 1;
                return <WrapPhoto>
                    <Image  bg={photo.sizes[size].url} />
                    <Link to={`/photo/${photo.id}`}>
                        <Text fs={14}>select</Text>
                    </Link>
                </WrapPhoto>
            })}
        </WrapPage>
    )
}

export default Album;