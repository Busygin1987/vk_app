import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { useHistory } from "react-router-dom";

import { Text } from '../../components/Typography';
import NavBar from '../../modules/NavBar';
import AlbumsList from './AlbumsList';

import { getAlbums, getAlbumPhoto } from './actions';

const WrapPage = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    flex-direction: column;
`;

const WrapText = styled.div`
    height: 80px;
    text-align: start;
    width: 100%;
    margin-left: 24px;
    padding: 24px;
`;

const Home = () => {
    const [ name, setName ] = React.useState('');
    const { albums, id } = useSelector(({ home: { albums }, auth: { user: { id } } }) => ({ albums, id }));
    const dispatch = useDispatch();
    const history = useHistory();

    const getAlbumPhotos = React.useCallback((albumId) => {
        dispatch(getAlbumPhoto.bind(null, id)(albumId));
        history.push(`/album/${albumId}`);
    }, [ dispatch, history, id ]);

    React.useEffect(() => {
        setName(localStorage.getItem('name'));
        dispatch(getAlbums());
    }, [ dispatch ]);

    return (
        <WrapPage>
            <NavBar />
            <WrapText>
                <Text>{name}</Text>
            </WrapText>
            <AlbumsList getPhotos={getAlbumPhotos} albums={albums} />
        </WrapPage>
    )
}

export default Home;