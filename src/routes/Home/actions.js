/* eslint-disable no-undef */
export const ALBUMS_REQUEST = 'ALBUMS_REQUEST';
export const ALBUMS_REQUEST_SUCCESS = 'ALBUMS_REQUEST_SUCCESS';
export const ALBUMS_REQUEST_FAIL = 'ALBUMS_REQUEST_FAIL';

export const PHOTOS_REQUEST = 'PHOTOS_REQUEST';
export const PHOTOS_REQUEST_SUCCESS = 'PHOTOS_REQUEST_SUCCESS';
export const PHOTOS_REQUEST_FAIL = 'PHOTOS_REQUEST_FAIL';

export const getAlbums = () => {
    return (dispatch) => {
        dispatch({
            type: ALBUMS_REQUEST,
        });

        VK.Api.call(
            'photos.getAlbums',
            { extended: 1, v: '5.80' },
            r => {
                const items = r.response.items;
                try {
                    dispatch({
                        type: ALBUMS_REQUEST_SUCCESS,
                        payload: items,
                    })
                } catch (e) {
                    dispatch({
                        type: ALBUMS_REQUEST_FAIL,
                        payload: new Error(e),
                    });
                }
            }
        )
    }
}

export const getAlbumPhoto = (owner_id, id) => {
    return (dispatch) => {
        dispatch({
            type: PHOTOS_REQUEST,
        });

        VK.Api.call(
            'photos.get',
            { extended: 1, v: '5.80', album_id: id, owner_id },
            r => {
                const items = r.response.items;
                try {
                    dispatch({
                        type: PHOTOS_REQUEST_SUCCESS,
                        payload: items,
                    })
                } catch (e) {
                    dispatch({
                        type: PHOTOS_REQUEST_FAIL,
                        payload: new Error(e),
                    });
                }
            }
        )
    }
}