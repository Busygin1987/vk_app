import React from 'react';
import styled from 'styled-components';

const Wrap = styled.div`
    flex: 1 1 auto;
    cursor: pointer;
`;

const Albums = ({ albums, getPhotos }) => {

    return (
        <Wrap>
            {albums.map((album) => {
                return <div key={album.id} onClick={() => getPhotos(album.id)}>{album.title}</div>
            })}
        </Wrap>
    )
}

export default Albums;