import { createReducer } from '../../store/utils/createReducer';
import {
    ALBUMS_REQUEST,
    ALBUMS_REQUEST_SUCCESS,
    ALBUMS_REQUEST_FAIL,
    PHOTOS_REQUEST,
    PHOTOS_REQUEST_SUCCESS,
    PHOTOS_REQUEST_FAIL
} from './actions';

const initialState = {
    error: '',
    login: false,
    albums: [],
    photos: [],
}

export default createReducer({
    [ALBUMS_REQUEST]: (state) => ({
        ...state,
        login: true
    }),
    [ALBUMS_REQUEST_SUCCESS]: (state, payload) => ({
        ...state,
        login: false,
        albums: payload,
    }),
    [ALBUMS_REQUEST_FAIL]: (state, payload) => ({
        ...state,
        error: payload,
        login: false,
    }),
    [PHOTOS_REQUEST]: (state) => ({
        ...state,
        login: true
    }),
    [PHOTOS_REQUEST_SUCCESS]: (state, payload) => ({
        ...state,
        login: false,
        photos: payload,
    }),
    [PHOTOS_REQUEST_FAIL]: (state, payload) => ({
        ...state,
        error: payload,
        login: false,
    }),
}, initialState);