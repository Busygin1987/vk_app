import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
    }

	html, body {
		box-sizing: border-box;
		width: 100%;
		height: 100vh;
		font-family: Arial, serif;
	}

	#root {
		user-select: none;
		height: 100%;
		width: 100%;
	}
`;