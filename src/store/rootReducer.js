import { combineReducers } from 'redux';
import auth from '../routes/Auth/reducer';
import home from '../routes/Home/reducer';

export default combineReducers({
    auth,
    home
})