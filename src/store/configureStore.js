import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import rootReducer from './rootReducer';

export default function configureStore() {

    const middlewares = [ thunk ];

    if (process.env.NODE_ENV !== 'production') {
        middlewares.push(logger);
    }
    // const devTools = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__() : null

    const store = createStore(
        rootReducer,
        compose(
            applyMiddleware(...middlewares),
            // devTools,
        )
    );

    return store
}