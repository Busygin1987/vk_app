import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Auth from './routes/Auth';
import Home from './routes/Home';
import Album from './routes/Album';
import Photo from './routes/Photo';
import Header from './modules/Header';
import PrivateRoute from './modules/PrivateRoute';

const App = () => (
  <Switch>
    <Route path='/'>
      <Header />
      <Switch>
        <Route exact path="/auth">
          <Auth />
        </Route>
        <PrivateRoute
          exact
          path="/album/:id"
          component={() => <Album />}
        />
        <PrivateRoute
          exact
          path="/photo/:id"
          component={() => <Photo />}
        />
        <PrivateRoute
          path="/"
          component={() => <Home />}
        />
      </Switch>
    </Route>
  </Switch>
);


export default App;