import React from 'react';
import styled from 'styled-components';

const WrapNavBar = styled.div`
    width: 100%;
    height: 80px;
    background-color: green;
`;

const NavBar = () => {
    return (
        <WrapNavBar>NavBar</WrapNavBar>
    )
}

export default NavBar