import React from 'react';
import styled from 'styled-components';

const WrapHeader = styled.div`
    height: 80px;
    width: 100%;
    background-color: rgba(0, 0, 0, .7);
`;

const Header = () => (
    <WrapHeader></WrapHeader>
);

export default Header;