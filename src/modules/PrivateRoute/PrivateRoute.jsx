import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props => (
            localStorage.getItem('logged_in')
                ? <Component {...props} />
                : <Redirect to="/auth" />
        )}
    />
);
export default PrivateRoute;