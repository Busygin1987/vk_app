import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import App from './App';
import configureStore from './store/configureStore';
import GlobalStyle from './globalStyle';

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
            <GlobalStyle />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);